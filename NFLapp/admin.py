# admin.py
from django.contrib import admin
from .models import Equipo, Jugador, Estadio


admin.site.register(Equipo)
admin.site.register(Jugador)
admin.site.register(Estadio)
