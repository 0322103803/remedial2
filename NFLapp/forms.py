from django import forms
from .models import Equipo, Jugador, Estadio

class BaseForm(forms.ModelForm):
    pass

class EquipoForm(BaseForm):
    class Meta:
        model = Equipo
        fields = ['nombre', 'ciudad', 'fundacion']
        widgets = {
            'nombre': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Nombre del equipo'}),
            'ciudad': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Ciudad'}),
            'fundacion': forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
        }

class JugadorForm(BaseForm):
    class Meta:
        model = Jugador
        fields = ['nombre', 'posicion', 'edad', 'equipo']
        widgets = {
            'nombre': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Nombre del jugador'}),
            'posicion': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Posición'}),
            'edad': forms.NumberInput(attrs={'type': 'number', 'class': 'form-control', 'placeholder': 'Edad'}),
            'equipo': forms.Select(attrs={'class': 'form-select'}),
        }

class EstadioForm(BaseForm):
    class Meta:
        model = Estadio
        fields = ['nombre', 'ciudad', 'capacidad']
        widgets = {
            'nombre': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Nombre del estadio'}),
            'ciudad': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Ciudad'}),
            'capacidad': forms.NumberInput(attrs={'type': 'number', 'class': 'form-control'}),
        }