# urls.py
# urls.py en la aplicación 'nfl'
from django.urls import path
from .views import *

app_name = 'nfl'

urlpatterns = [
    path('equipos/', ListEquipos.as_view(), name='list_equipos'),
    path('equipos/<int:pk>/', DetailEquipo.as_view(), name='detail_equipo'),
    path('equipos/nuevo/', CreateEquipo.as_view(), name='create_equipo'),
    path('equipos/editar/<int:pk>/', UpdateEquipo.as_view(), name='update_equipo'),
    path('equipos/eliminar/<int:pk>/', DeleteEquipo.as_view(), name='delete_equipo'),
    path('estadios/', ListEstadios.as_view(), name='list_estadios'),
    path('estadios/<int:pk>/', DetailEstadio.as_view(), name='detail_estadio'),
    path('estadios/nuevo/', CreateEstadio.as_view(), name='create_estadio'),
    path('estadios/editar/<int:pk>/', UpdateEstadio.as_view(), name='update_estadio'),
    path('estadios/eliminar/<int:pk>/', DeleteEstadio.as_view(), name='delete_estadio'),
    path('jugadores/', ListJugadores.as_view(), name='list_jugadores'),
    path('jugadores/<int:pk>/', DetailJugador.as_view(), name='detail_jugador'),
    path('jugadores/nuevo/', CreateJugador.as_view(), name='create_jugador'),
    path('jugadores/editar/<int:pk>/', UpdateJugador.as_view(), name='update_jugador'),
    path('jugadores/eliminar/<int:pk>/', DeleteJugador.as_view(), name='delete_jugador'),
]