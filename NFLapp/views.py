from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Equipo, Jugador, Estadio
from .forms import EquipoForm, JugadorForm, EstadioForm 

# CRUD para Equipos

class CreateEquipo(LoginRequiredMixin, generic.CreateView):
    template_name = "nfl/equipos/create_equipo.html"
    model = Equipo
    form_class = EquipoForm
    success_url = reverse_lazy('nfl:list_equipos')
    login_url = reverse_lazy("home:login")


class ListEquipos(LoginRequiredMixin, generic.ListView):
    template_name = "nfl/equipos/list_equipos.html"
    model = Equipo
    login_url = reverse_lazy("home:login")

class DetailEquipo(LoginRequiredMixin, generic.DetailView):
    template_name = "nfl/equipos/detail_equipo.html"
    model = Equipo
    login_url = reverse_lazy("home:login")

class UpdateEquipo(LoginRequiredMixin, generic.UpdateView):
    template_name = "nfl/equipos/update_equipo.html"
    model = Equipo
    form_class = EquipoForm
    success_url = reverse_lazy('nfl:list_equipos')
    login_url = reverse_lazy("home:login")

class DeleteEquipo(LoginRequiredMixin, generic.DeleteView):
    template_name = "nfl/equipos/delete_equipo.html"
    model = Equipo
    success_url = reverse_lazy('nfl:list_equipos')
    login_url = reverse_lazy("home:login")


# CRUD para Jugadores

class CreateJugador(LoginRequiredMixin, generic.CreateView):
    template_name = "nfl/jugadores/create_jugador.html"
    model = Jugador
    form_class = JugadorForm
    success_url = reverse_lazy('nfl:list_jugadores')
    login_url = reverse_lazy("home:login")

class ListJugadores(LoginRequiredMixin, generic.ListView):
    template_name = "nfl/jugadores/list_jugadores.html"
    model = Jugador
    login_url = reverse_lazy("home:login")

class DetailJugador(LoginRequiredMixin, generic.DetailView):
    template_name = "nfl/jugadores/detail_jugador.html"
    model = Jugador
    login_url = reverse_lazy("home:login")

class UpdateJugador(LoginRequiredMixin, generic.UpdateView):
    template_name = "nfl/jugadores/update_jugador.html"
    model = Jugador
    form_class = JugadorForm
    success_url = reverse_lazy('nfl:list_jugadores')
    login_url = reverse_lazy("home:login")

class DeleteJugador(LoginRequiredMixin, generic.DeleteView):
    template_name = "nfl/jugadores/delete_jugador.html"
    model = Jugador
    success_url = reverse_lazy('nfl:list_jugadores')
    login_url = reverse_lazy("home:login")


# CRUD para Estadios

class CreateEstadio(LoginRequiredMixin, generic.CreateView):
    template_name = "nfl/estadios/create_estadio.html"
    model = Estadio
    form_class = EstadioForm
    success_url = reverse_lazy('nfl:list_estadios')
    login_url = reverse_lazy("home:login")

class ListEstadios(LoginRequiredMixin, generic.ListView):
    template_name = "nfl/estadios/list_estadios.html"
    model = Estadio
    login_url = reverse_lazy("home:login")

class DetailEstadio(LoginRequiredMixin, generic.DetailView):
    template_name = "nfl/estadios/detail_estadio.html"
    model = Estadio
    login_url = reverse_lazy("home:login")

class UpdateEstadio(LoginRequiredMixin, generic.UpdateView):
    template_name = "nfl/estadios/update_estadio.html"
    model = Estadio
    form_class = EstadioForm
    success_url = reverse_lazy('nfl:list_estadios')
    login_url = reverse_lazy("home:login")

class DeleteEstadio(LoginRequiredMixin, generic.DeleteView):
    template_name = "nfl/estadios/delete_estadio.html"
    model = Estadio
    success_url = reverse_lazy('nfl:list_estadios')
    login_url = reverse_lazy("home:login")



