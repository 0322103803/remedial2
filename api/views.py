from django.shortcuts import render, redirect

# Create your views here.

from django.views import generic
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response

from core.models import GrantGoal
from .forms import CreateGrantGoalForm
from .serializers import GrantGoalSerializer, GrantGoalSerializerCreate


class GrantGoalListAPIView(generics.ListAPIView):
    queryset = GrantGoal.objects.filter(status=True)
    serializer_class = GrantGoalSerializer



    
class GrantGoalDetailAPIView(APIView):
   def get(self, request, pk, *args, **kwargs):
       queryset = GrantGoal.objects.get(pk=pk)
       data = GrantGoalSerializer(queryset).data
       return Response(data)
   


class GrantGoalCreateAPIView(generics.CreateAPIView):
    serializer_class = GrantGoalSerializerCreate



   ##### CLIENTS ######
import requests

class ListGrantGoalClient(generic.View):
    template_name = "api/list_client.html"
    context = {}
    url_api = "http://localhost:8000/api/v2/list/grantgoal/"

    def get(self, request):
        response = requests.get(url=self.url_api)
        self.context = {
            "grantgoals": response.json()
        }
        return render(request, self.template_name, self.context)


class CreateGrantGoalClient(generic.View):
    template_name = "api/create_client.html"
    context = {}
    url_api = "http://localhost:8000/api/v2/create/grantgoal/"
    form_class = CreateGrantGoalForm
    response = None
    payload = {}

    def get(self, request):
        self.context = {
            "form": self.form_class
        }
        return render(request, self.template_name, self.context)
    

    def post(self, request, *args, **kwargs):
        self.payload = {            
            "grantgoal_name": request.POST["grantgoal_name"],
            "description": request.POST["description"],
            "days_duration": request.POST["days_duration"],
            "status": request.POST["status"],
            "state": request.POST["state"],
            "slug": request.POST["slug"],
            "user": request.POST["user"]
        }
        print("-------", self.payload)
        self.response = requests.post(url=self.url_api, data=self.payload)
        self.context = {
            "form": self.form_class
        }
        return redirect("api:client_list")